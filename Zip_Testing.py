#!/usr/bin/env python3

import pdb
import zipfile
import re
import win32clipboard

zf = zipfile.ZipFile('NewEnvAllBarcodes.xps', 'r')


def get_fpage_list():
    """returns utf-8 list of strings representing fpage files"""

    fpage_list = []
    for name in zf.namelist():
        if name.endswith('.fpage'):
            fpage_list.append(zf.read(name).decode('utf-8'))

    return fpage_list


def get_all_unicode_strings(fpage_list):
    """returns list of unicode match string lists"""

    uni_reg = re.compile('(?<=UnicodeString\=\").*(?=\")')
    uni_match_string_list = [re.findall(uni_reg,f) for f in fpage_list]

    return uni_match_string_list


def get_all_indices_strings(fpage_list):
    """returns list of indices match string lists"""

    ind_reg = re.compile('(?<=Indices\=\").*(?=\" U)')
    ind_match_string_list = [re.findall(ind_reg,f) for f in fpage_list]

    return ind_match_string_list


def get_int_indices_lists(ind_match_string_list):
    """returns list of indices int tuple lists for each unicodestring in each fpage of the xps doc"""

    ind_string_pairs_list = []

    for string_list in ind_match_string_list:
        indice_strings = [ind_str.split(';') for ind_str in string_list]
        ind_string_pairs_list.append(indice_strings)

    paged_ind_int_tuples = []
    for page in ind_string_pairs_list:
        ind_int_tuples_lists = []
        for string_indices in page:
            ind_int_tuples = []
            for pair_indices in string_indices:
                ind_int_tuples.append(tuple(int(i) for i in pair_indices.split(',')))
            ind_int_tuples_lists.append(ind_int_tuples)
        paged_ind_int_tuples.append(ind_int_tuples_lists)

    return paged_ind_int_tuples


def get_column_break_lists(ind_int_tuples_lists):
    """returns list of column break string indices lists

    Will most likely need to put something in to catch any unicodestrings, that
    will not have any column breaks, such as the beginning one ex:

    Unicode_String = #
    Indices = (6,)

    Possibly append a -1 to indicate no column breaks are present for the current string. """

    paged_col_break_index_lists = []

    for page in ind_int_tuples_lists:
        col_breaks_list = []
        for indice_list in page:
            col_breaks = []
            i = 0
            while i < len(indice_list):

                if len(indice_list[i]) == 2 and indice_list[i][1] >= 200:
                    col_breaks.append(i)
                i += 1
            if col_breaks:
                col_breaks_list.append(col_breaks)
            else:
                col_breaks_list.append(-1234)

        paged_col_break_index_lists.append(col_breaks_list)

    return paged_col_break_index_lists


def get_cell_values_lists(uni_string_lists, col_break_index_lists):
    """returns list of cell value lists

    Needs rewriting to traverse the lists and sublists properly"""

    paged_cell_vals_lists = []

    for uni_list_page,col_list_page in zip(uni_string_lists,col_break_index_lists):
        cell_vals = []
        counter = 0

        while counter < len(uni_list_page):

            if isinstance(col_list_page[counter], list):
                i = 0 #because Osmond sez so
                for col_breaks in col_list_page[counter]:
                    cell_vals.append(uni_list_page[counter][i:col_breaks+1])
                    i = col_breaks + 1
                cell_vals.append(uni_list_page[counter][col_breaks+1:])
            else:
                if col_list_page[counter] == -1234:
                    cell_vals.append(uni_list_page[counter])
                else:
                    cell_vals.append(uni_list_page[counter][i:col_breaks+1])
                    i = col_breaks + 1
                    cell_vals.append(uni_list_page[counter][col_breaks+1:])
            counter += 1

        paged_cell_vals_lists.append(cell_vals)

    return paged_cell_vals_lists


def test_run():
    fpage_list = get_fpage_list()
    uni_match_string_list = get_all_unicode_strings(fpage_list)
    ind_match_string_list = get_all_indices_strings(fpage_list)
    ind_int_tuples_list = get_int_indices_lists(ind_match_string_list)
    col_break_index_lists = get_column_break_lists(ind_int_tuples_list)
    cell_vals_lists = get_cell_values_lists(uni_match_string_list,col_break_index_lists)
    for p in cell_vals_lists:
        print(p)
    print('Number of fpages: ' + str(len(cell_vals_lists)))
    pdb.set_trace()
test_run()